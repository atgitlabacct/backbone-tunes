window.Playlist = Albums.extend({
  isFirstAlbum: function(index){
    return index == 0;
  },

  isLastAlbum: function(index){
    return (index == (this.models.length - 1));
  }
});
