window.LibraryAblumView = AlbumView.extend({
  events: {
    'click .queue.add': 'select'
  },

  select: function(){
    this.collection.trigger('select', this.model);
    console.log("Triggered select", this.model);
  },
});
