window.PlaylistView = Backbone.View.extend({
  tagName: 'section',
  className: 'playlist',

  initialize: function() {
    _.bindAll(this, 'render');
    this.template = _.template($('#playlist-template').html())
    this.collection.bind('reset', this.render);

    this.player = this.options.player;

    this.library = this.options.library;
  },

  render: function(){
    $(this.el).html(this.template(this.player.toJSON()));

    this.$('button.play').toggle(this.player.isStopped());
    this.$('button.pause').toggle(this.player.isPlaying());

    return this;
  }
});
