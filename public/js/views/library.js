window.LibraryView = Backbone.View.extend({
  tagName: 'section',
  className: 'library',

  initialize: function() {
    _.bindAll(this, 'render');
    this.template = _.template($('#library-template').html());
    this.collection.bind('reset', this.render);
  },

  render: function() {
    var $albums,
      collection = this.collection;

    $(this.el).html(this.template({}));
    $albums = this.$('.albums'); // Get the jquery object of class=albums

    collection.each(function (album) {
      var view = new LibraryAblumView({
        model: album,
        collection: collection
      });
      $albums.append(view.render().el); // Use the jquery object and append the library album view template
    });
    return this;
  }
});


