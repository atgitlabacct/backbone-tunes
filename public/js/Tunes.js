(function($) {
  window.library = new Albums();
  window.player  = new Player();

  $(function() {
    window.App = new BackboneTunes();
    Backbone.history.start({
      pushState: true
    });
  });
})(jQuery);
